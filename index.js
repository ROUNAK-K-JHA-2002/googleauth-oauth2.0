const express = require('express');
const session = require('express-session');
const passport = require('passport')
const app = express();

require('./middleware')



app.use(session({ secret: 'cats', resave: false, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

function isLoggedIn(req, res, next) {
  req.user ? next() : res.sendStatus(401);
}

app.get('/', (req, res) => {
  res.send('<a href="/auth/google">Authenticate with Google</a>');
});

app.get('/auth/google',
  passport.authenticate('google', { scope: [ 'email', 'profile' ] }
));

app.get( '/auth/google/callback',
  passport.authenticate( 'google', {
    successRedirect: '/protected',
    failureRedirect: '/auth/google/failure'
  })
);

app.get('/protected', isLoggedIn, (req, res) => {
  res.send(`<div style="text-align:center;">
         <p>Hello , ${req.user.displayName}</p>
         <p>Your email Id is ${req.user.email}</p>
        
  </div>`);
});

app.get('/logout', (req, res) => {
  req.logout(function(err) {
               if (err) { return next(err); }
               req.session.destroy();
           res.redirect('/');
  });
});

app.get('/auth/google/failure', (req, res) => {
  res.send('Failed to authenticate..');
});

app.listen(5000, () => console.log('server running on http://localhost:5000'));